const fetch = require('cross-fetch');

class piaInterface{
    async login(baseUrl, username, password) {
        const res = await fetch(baseUrl + '/api/v1/user/login', {
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                username: username,
                password: password,
                locale: 'de-DE',
                logged_in_with: 'web',
            }),
              method: 'POST',
        });
        if (res.ok) {
            return (await res.json()).token;
        } else {
            throw new Error(res.status);
        }
    }
    
    async addEmail(baseUrl, pmToken, pseudonym, email) {
      const res = await fetch(
        baseUrl + `/api/v1/personal/personalData/proband/${pseudonym}`,
        {
          headers: {
            accept: 'application/json',
            authorization: pmToken,
            'content-type': 'application/json',
          },
          body: JSON.stringify({ email: email }),
          method: 'PUT',
        }
      );
      if (!res.ok) {
        throw new Error(res.status);
      }
    }
}

let pia = new piaInterface();
module.exports = pia